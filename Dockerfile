FROM node:14.8.0-alpine
WORKDIR /apps/whomongo
COPY . .
RUN npm i
RUN npm i -g @babel/cli