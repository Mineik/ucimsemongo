import sha256 from "crypto-js/sha256"

/**
 * this presents unified way to encrypt data in this web application.
 * @param password
 * @return passwordHas encrypted password using sha256
 */
export function encryptPassword(password){
	return sha256(/*TODO dan 2020-10-15: add some salt (salt from .env)*/ password).toString()
}