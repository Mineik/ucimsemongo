import {getUser, newUser, verifyIfUserExists, verifyUserAgainstDb} from "../documentVerifier/userAuth";
import {encryptPassword} from "../encryptor";
import {JWK, JWT} from "jose"
import {logger} from "../logger/logger";
import {User, userTokenPayload} from "./user";

export async function registerUser(user){
	if(await verifyIfUserExists(user.email) == true){
		await newUser(user.username, encryptPassword(user.password), user.email, user.name ? user.name : user.username).catch(e=>logger(2, e.message))
		return true
	}else{
		return false
	}
}

export async function loginUser(username, passwordHash): Promise<string|boolean>{
	if(await verifyUserAgainstDb(username, passwordHash)){
		const user = await getUser(null, username).catch(e=>logger(3, e.message))
		try{
			if(user != null) return keygen(user)
		}catch(e){
			return false
		}
	}
	return false
}

export async function verifyLogin(token): Promise<boolean>{
	const user: User = await getUser((JWT.decode(token) as userTokenPayload).uid)
	const verifiedKey: userTokenPayload = JWT.verify(token, JWK.asKey(user.userKey)) as userTokenPayload
	return verifiedKey.uid == user.uid
}

export function keygen(user, testData?: {
	now: Date,
	expiresIn,
}, type?: string){
	let token = ""
	try{
		token = JWT.sign({
			unm: user.username,
			din: user.displayName,
			avh: user.avatarHash,
			uid: user.uid,
			t: type? type:"auth"
		}, 
		JWK.isKey(user.userKey)? user.userKey:JWK.asKey(user.userKey,{calculateMissingRSAPrimes: true}),
		{
			now: testData?.now,
			expiresIn: testData?.expiresIn
		})
	}catch(e){
		logger(2, e.message)
	}

	if(token != "") return token
	else throw "cannot generate token"
}