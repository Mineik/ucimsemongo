import {Router} from "express";
import {loginUser, registerUser} from "./authorizer";
import {encryptPassword} from "../encryptor";


const authRouter = Router()


authRouter.get("/user?u=:u", (req, res, next)=>{
	res.status(501)
	res.jsonp({
		message: "Not implemented yet"
	})

	next()
})

authRouter.post("/user", async (req, res, next)=>{ //register
	const authHeader = Buffer.from(req.get("Authorization"), "base64")
	if(req.hostname != 'localhost' && req.hostname != process.env.REMOTEHOST){ //if we authorize from foreign host, you have to use API key to do an auth on backend
		if(authHeader.toString("ascii").split(" ")[0] == "Bearer"){
			//token verifictaion logic
		}else{
			res.status(401)
			res.jsonp({
				message: "Unauthorized"
			})
		}
	}else{
		const reg = await registerUser(req.body)
		res.status(reg? 200:403)
		res.jsonp(reg?{
			message: "registration successfull"
		}:{
			message: "user already exists"
		})
	}
	next()
})

authRouter.patch("/user", (req,res,next)=>{ //update and generate new token
	res.status(451)
	res.jsonp({
		message: "Not implemented"
	})
	next()
})

authRouter.get("/user", async (req,res,next)=>{ //login
	const authHead = req.get("Authorization").split(" ")
	const authType = authHead[0]
	const authBody = Buffer.from(authHead[1], "base64").toString()
	if(authType == "Basic"){
		const creds = authBody.split(":")
		const login =await  loginUser(creds[0], encryptPassword(creds[1]))
		if(login !== false){
			res.jsonp({
				token: login,
				message: "successful login"
			})
			res.status(200)
		}
		else{
			res.jsonp({
				message: "Unauthorized"
			})
			res.status(401)
		}

	}else{
		res.jsonp({
			message: "Unauthorized"
		})
		res.status(401)
	}
	next()
})

authRouter.delete("/user", (req,res,next)=>{ //delete user
	res.status(451)
	res.jsonp({
		message: "Not implemented"
	})
	next()
})

export{authRouter}