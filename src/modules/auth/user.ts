export class User{
	username: string
	email: string
	password?: string
	uid: string
	avatarHash: string
	displayName: string
	userKey
}

export class userTokenPayload{
	unm: string
	din: string
	avh: string
	uid: string
	t: string
}