import {logGQLReq} from "./logger";

export const loggerApolloPlugin ={
	requestDidStart(){
		let success = true
		// logger(1, requestContext.request?.query)
		return{
			didEncounterErrors(){
				success = false
			},
			willSendResponse(){
				logGQLReq(success)
			}
		}
	}
}