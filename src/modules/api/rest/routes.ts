import {Router, json} from "express"
import {logger} from "../../logger/logger";
import {getCollectionData} from "../../database/getCollectionData";
import {getSpecificDoc} from "../../database/getSpecificDoc";
import addMessage from "../../documentVerifier/message";
import {loggerMiddleware} from "../../logger/expressMiddleware";
import {authRouter} from "../../auth/authRouter";
import {verifyLogin} from "../../auth/authorizer";
import cors from "cors";


const router = Router()
router.use(json())
router.use(cors())
router.use("/auth", authRouter)


router.get('/ping', (req,res, next)=>{
	res.status(200)
	res.json({
		message: "hello world!"
	})
	next()
})

router.get('/testTableData', async (req, res, next)=>{
	try{
		res.json(await getCollectionData("test_database","test"))
		res.status(200)
	}catch(e){
		logger(2, e)
		res.status(503)
		res.jsonp(e)
	}
	next()
})

router.get("/testMessage", async (req, res, next)=>{
	try{
		const doc = await getSpecificDoc("test_database","test",{addedOn: "2020-10-15"})
		res.status(200).jsonp(doc)
	}catch (e){
		res.status(503).json(e)
	}
	next()
})

router.post("/message", async (req,res,next)=>{
	logger(1, JSON.stringify(req.body))
	addMessage(req.body.message).then(async result=>{
		try{
			res.status(200)
			res.jsonp(await getSpecificDoc("test_database","test",{_id: result.insertedId}))
		}catch(e){
			res.status(503)
			res.jsonp(e)
		}
		next()
	})
})

router.get("/message", async (req,res,next)=>{
	res.status(501)
	res.jsonp({message: "Not implemented"})
	next()
})

router.get("/superSecret", async (req,res,next)=>{
	const authHead = req.get("Authorization")?.split(" ")
	if(authHead[0] == "Bearer"){
		const isLoginValid = await verifyLogin(authHead[1])
		res.status(isLoginValid? 200:401).json(isLoginValid? {
			message: "Super secret message"
		}: {
			message: "Unauthorized"
		})
	}else{
		res.status(401).json({message:"Unauthorized"})
	}
	next()
})

router.use("/**", (req,res, next)=>{
	if(!res.headersSent){
		res.status(404)
		res.json({
			message: "Not Found"
		})
	}
	next()
})
router.use(loggerMiddleware)
export {router as apiRouter}
