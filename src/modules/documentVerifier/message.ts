import {addDocToCollection} from "../database/addDocToCollection";

export default async function addMessage(message: string){
	const Timestamp = new Date()
	const timestamp = `${Timestamp.getFullYear()}-${Timestamp.getMonth()+1<10 ?`0${Timestamp.getMonth()+1}`:Timestamp.getMonth()+1}-${Timestamp.getDate()<10? `0${Timestamp.getDate()}`:Timestamp.getDate()}`

	return await addDocToCollection("test_database", "test", {
		message: message,
		addedOn: timestamp
	})
}