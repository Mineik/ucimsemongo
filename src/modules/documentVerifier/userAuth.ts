import {addDocToCollection} from "../database/addDocToCollection";
import {getSpecificDoc} from "../database/getSpecificDoc";
import {User} from "../auth/user";
import gravatar from "gravatar"
import {JWK} from "jose";
import {ObjectId} from "mongodb"

/**
 * Collection of utilites to be in middle of database driver and authorization module
 */
/**
 * Adds user to database
 * @param user username to register
 * @param passwdHash hashed password
 * @param email user email
 * @param name user's display name, if not provided is replaced with username
 */
export async function newUser(user, passwdHash, email, name){
	const avatarHash = gravatar.url(email, {
		d: "retro",
		s: 250
	}).replace(/\/\/www\.gravatar.com\/avatar\//gm,"")
	await addDocToCollection("test_database","users",{
		username: user,
		password: passwdHash,
		displayName: name,
		email,
		avatarHash,
		userKey: JWK.generateSync("RSA").toJWK(true)
	})
}

/**
 * Verify if user's email is already in database.
 * @param email user's email
 */
export async function verifyIfUserExists(email): Promise<boolean>{
	try {
		const user = await getSpecificDoc("test_database","users", {email})
		if(user == false) return false
	}catch(e){
		return false
	}
	return true
}

/**
 * Get one user and all his data in by either his uid or username
 * @param uid
 * @param username
 * @return User user object
 * @return null user doesn't exist
 */
export async function getUser(uid?, username?): Promise<User|null>{
	if(uid && !username){
		const user = await getSpecificDoc("test_database","users",{"_id": new ObjectId(uid)})
		user.uid = user._id
		return user
	}
	else if(!uid && username){
		const user =  await getSpecificDoc("test_database","users",{username})
		user.uid = user._id
		return user
	}
	else if(uid && username){
		const user =  await getSpecificDoc("test_database","users", {"_id": new ObjectId(uid), username})
		user.uid = user._id
		return user
	}
	else{
		return null
	}
}


/**
 * loggin middleman, tries to get document
 * @param username user's username
 * @param passwordHash sha256 hash of user's password
 */
export async function verifyUserAgainstDb(username, passwordHash): Promise<boolean>{
	return await getSpecificDoc("test_database", "users", {username, password: passwordHash}) != null
}
