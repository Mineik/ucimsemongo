import Vue from "vue"
import Index from "./pages/index.vue"
import App from "./pages/app.vue"
import Login from "./pages/login.vue"
import Apollo from "vue-apollo"
import ApolloClient from "apollo-boost";
import VueRouter from "vue-router"

const client = new ApolloClient({
	uri: `http://localhost:3000/graphql`
})
const provider = new Apollo({
	defaultClient: client
})

const router = new VueRouter({
	routes: [
		{
			path: "/",
			component: Index
		},
		{
			path: "/login",
			component: Login
		}
	]
})

Vue.use(Apollo)
Vue.use(VueRouter)

new Vue({
	apolloProvider: provider,
	router,
	render: createElement => createElement(App)
}).$mount("#app")