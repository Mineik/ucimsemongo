import {keygen} from "../src/modules/auth/authorizer";
import {encryptPassword} from "../src/modules/encryptor";
import {JWK} from "jose";

test("Generate token", ()=>{
	const token = keygen({
		username: "dan",
		email: "dan@hudebniciulicnici.eu",
		avatarHash: encryptPassword("dan@hudebniciulicnici.eu"),
		uid: "0001",
		displayName: "Dan z hudebníci uličníci",
		userKey: JWK.None
	}, {
		now: new Date("2020-10-15T11:42:00"),
		expiresIn: "10m"
	})
	expect(token.length).toBe(261)
})
