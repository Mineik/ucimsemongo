import {client, connectToClient, disconnectFromClient} from "../src/modules/database";
import {loginUser, registerUser, verifyLogin} from "../src/modules/auth/authorizer";
import {encryptPassword} from "../src/modules/encryptor";
import {JWK} from "jose";

describe("Authorization module", ()=>{
	let token;
	beforeAll(async ()=>{
		await connectToClient()
	})

	it("should be able to register user", async ()=>{
		const userRegistration = await registerUser({
			username: "test",
			email: "test@test.test",
			uid: "test",
			avatarHash: encryptPassword("password"),
			password: "password",
			displayName: "Pan test",
			userKey: JWK.None
		})

		expect(userRegistration).toBeTruthy()
	})

	it("should be able to log user in and provide him with token with right password", async ()=>{
		const userLogin = await loginUser("test", encryptPassword("password"))
		token = userLogin
		expect(typeof userLogin).toBe("string")
	})

	it("should be able to return false if password is wrong", async ()=>{
		const userLogin = await loginUser("test", "baddpasswd")
		expect(userLogin).toBeFalsy()
	})

	it("should be able to verify that the token belongs to user", async ()=>{
		const userTest = verifyLogin(token)
		expect(userTest).toBeTruthy()
	})

	afterAll(()=> {
		client.db("test").collection("users").deleteMany({"username":"test"}).then(()=>{
			disconnectFromClient().then(()=> {
				return
			})
		})
	})
})
