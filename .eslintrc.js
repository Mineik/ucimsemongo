module.exports ={
	root: true,
	parser: "@typescript-eslint/parser",
	plugins: [
		"@typescript-eslint"
	],
	extends: [
		"eslint:recommended",
		"plugin:@typescript-eslint/eslint-recommended",
		"plugin:@typescript-eslint/recommended"
	],
	rules: {
		indent: [2, "tab",],
		"@typescript-eslint/ban-ts-comment":0,
		"@typescript-eslint/explicit-module-boundary-types":0,
		"no-console": 2
	}
}